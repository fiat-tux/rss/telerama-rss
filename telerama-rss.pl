#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::Collection;
use Mojo::DOM;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(decode encode);
use XML::RSS;
use Data::Dumper;

my $login = decode('UTF-8', $ENV{TELERAMA_LOGIN});
my $pwd   = decode('UTF-8', $ENV{TELERAMA_PWD});
my $file  = $ENV{TELERAMA_RSSFILE};
my $cat   = $ENV{TELERAMA_CATEGORY};

my $cats = Mojo::Collection->new('une', 'cinema', 'ecrans', 'enfants', 'sortir', 'musique', 'radio', 'livre', 'debats-reportages');

die 'Please set TELERAMA_LOGIN, TELERAMA_PWD, TELERAMA_RSSFILE and TELERAMA_CATEGORY. Aborting.' unless ($login && $pwd && $file && $cat);

die 'TELERAMA_CATEGORY must be "une", "cinema", "ecrans", "enfants", "sortir", "musique", "radio", "livre" or "debats-reportages"' unless $cats->grep(sub { $_ eq $cat })->size;

my $url = Mojo::URL->new('https://www.telerama.fr/rss/');

$url->path->merge($cat.'.xml');

my $ua   = Mojo::UserAgent->new();
   $ua->max_redirects(4);

# Connection
my $args = {
    email    => $login,
    password => $pwd,
};

$ua->get('https://sec.telerama.fr/sfuser/connexion');
$ua->post('https://sec.telerama.fr/sfuser/connexion' => form => $args);

my $feed = XML::RSS->new();

# Get articles
$feed->parse($ua->get($url)->result->body);

$cat = '' if $cat eq 'une';
$feed->channel(link => Mojo::URL->new('https://www.telerama.fr/'.$cat));

foreach my $item (@{$feed->{'items'}}) {
    my $link        = Mojo::URL->new($item->{link});
    my $description = '';

    # Fix unvalid link
    $link =~ s@https://www\.telerama\.frhttp@http@;
    $link =~ s@https://www\.telerama\.frtlrm:/@https://www.telerama.fr@;

    my $page = decode('UTF-8', $ua->get($link)->result->body);
    if ($page) {
        my $dom  = Mojo::DOM->new($page);
        if ($dom) {
            $dom->find('span.score, li.sheet__notation')->each(sub {
                my ($e, $num) = @_;
                if ($e->text eq 'o') {
                    $e->content('😒 / - / Hélas');
                } elsif ($e->text eq 'p') {
                    $e->content('😑 / T / Bof');
                } elsif ($e->text eq 'q') {
                    $e->content('🙂 / TT / Bien');
                } elsif ($e->text eq 'r') {
                    $e->content('😀 / TTT / Très bien');
                } elsif ($e->text eq 's') {
                    $e->content('🥰 / TTTT / Bravo');
                }
            });
            $dom->find('.sheet__header-img-movie')->each(sub {
                my ($e, $num) = @_;
                $description .= $e->to_string if $e->to_string;
            });
            $dom->find('.sheet__text-container')->each(sub {
                my ($e, $num) = @_;
                $description .= $e->content if $e->content;
            });
            $dom->find('.publication__card')->each(sub {
                my ($e, $num) = @_;
                $description .= $e->content if $e->content;
            });
            $dom->find('.media__picture[loading="eager"]')->each(sub {
                my ($e, $num) = @_;
                $description .= $e->to_string if $e->to_string;
            });
            $dom->find('.article__page-header .video__iframe')->each(sub {
                my ($e, $num) = @_;
                $description .= $e->to_string if $e->to_string;
            });
            $dom->find('.article__page-content')->each(sub {
                my ($e, $num) = @_;
                $e->find('noscript > img')->each(sub {
                    my ($f, $num) = @_;
                    $f->parent->replace($f);
                });
                $e->find('noscript > iframe')->each(sub {
                    my ($f, $num) = @_;
                    $f->parent->replace($f);
                });
                $e->find('img[fetchpriority="high"]')->each(sub {
                    my ($f, $num) = @_;
                    $f->remove;
                });
                $e->find('img[data-src]')->each(sub {
                    my ($f, $num) = @_;
                    $f->remove;
                });
                $e->find('iframe[data-src]')->each(sub {
                    my ($f, $num) = @_;
                    $f->remove;
                });
                $e->find('#js-sharing')->each(sub {
                    my ($f, $num) = @_;
                    $f->remove;
                });
                $dom->find('.publication__card')->each(sub {
                    my ($f, $num) = @_;
                    $f->remove;
                });
                $description .= $e->content if $e->content;
            });
        }
        if ($description) {
            $description =~ s/www\.youtube\.com/www.youtube-nocookie.com/g;
            $description =~ s/allow="([^"]*)autoplay([^"]*)"/allow="$1$2"/g;
            $item->{description} = $description;
        }
    }
}
$feed->save($file);
